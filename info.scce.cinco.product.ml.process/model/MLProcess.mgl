import "platform:/resource/info.scce.cinco.product.ml.process/model/JupyterFunctions.ecore" as functionLibrary

@mcam("check")
@mcam_checkmodule("info.scce.cinco.product.ml.process.checks.CheckConstantPorts")
@mcam_checkmodule("info.scce.cinco.product.ml.process.checks.CheckDataFlowTypes")
@mcam_checkmodule("info.scce.cinco.product.ml.process.checks.CheckDataFlowExists")
@mcam_checkmodule("info.scce.cinco.product.ml.process.checks.CheckSynchronizedFunctions")

@pyroEcoreRootType("FunctionGroup")
@pyroEcoreExcludeType("Parameter")

@contextMenuAction("info.scce.cinco.product.ml.process.ExecuteML")
@contextMenuAction("info.scce.cinco.product.ml.process.actions.CreateInputsOutputsContextMenu")
@postCreate("info.scce.cinco.product.ml.process.MLProcessPostCreate")
@style("model/MLProcess.style")
graphModel MLProcess {
	package info.scce.cinco.product.ml.process
	nsURI "http://cinco.scce.info/product/mlprocess"
	iconPath "icons/mlprocess_icon_smallabstract.png"
	diagramExtension "ml"
	
	containableElements (
		Inputs[0,1],
		Outputs[0,1],
		DataService[0,*],
		ConstantPort[0,*]
	)
	attr EString as name
	attr EString as documentation

	abstract container Closure {}

	@style(inputs)
	@disable(resize)
	@postCreate("info.scce.cinco.product.ml.process.hooks.CreateInferredPort_PostCreate")
	@doubleClickAction("info.scce.cinco.product.ml.process.CreateInferredPort")
	@palette("Closure")
	container Inputs extends Closure { 
		containableElements(StartOutputPort [0,*])
	}

	@style(outputs)
	@disable(resize)
	@postCreate("info.scce.cinco.product.ml.process.hooks.CreateInferredPort_PostCreate")
	@doubleClickAction("info.scce.cinco.product.ml.process.CreateInferredPort")
	@palette("Closure")
	container Outputs extends Closure {
		containableElements(EndInputPort [0,*])
	}

	abstract container DataService {
		containableElements(TypedPort)
	}

	@style(externalservice, "${fun.name}")
	@disable(resize)
	@contextMenuAction("info.scce.cinco.product.ml.process.ServiceUpdate")
	@postCreate("info.scce.cinco.product.ml.process.ServicePostCreate")
	container ExternalService extends DataService {
		@pyroInformation
		attr EString as documentation
		prime functionLibrary.Function as fun
		
	}

	@style(internalservice, "${proMod.name}")
	@disable(resize)
	@jumpToPrime
	@contextMenuAction("info.scce.cinco.product.ml.process.ServiceUpdate")
	@postCreate("info.scce.cinco.product.ml.process.ServicePostCreate")
	container InternalService extends DataService {
		@pyroInformation
		attr EString as documentation
		prime this::MLProcess as proMod
	}
	
	

	abstract node Port {
		
	}
	
	abstract node TypedPort extends Port {
	}
	
	abstract node InferredPort extends Port {
		attr EString as name 
	}

	/*
	 * Function depending Ports
	 */
	@style(inputPort, "${parameter.name}:${parameter.typeName}")
	@disable(create,delete,resize,move)
	node InputPort extends TypedPort {
		prime functionLibrary.Parameter as parameter
		incomingEdges ({DataFlow,InferredStartDataFlow}[0,1])
	}
	
	@style(outputPort, "${parameter.name}:${parameter.typeName}")
	@disable(create,delete,resize,move)
	node OutputPort extends TypedPort {
		prime functionLibrary.Parameter as parameter
		outgoingEdges ({DataFlow,InferredEndDataFlow}[0,*])
	}
	
	/*
	 * User definable Ports
	 * Types are inferred
	 */
	
	@style(outputPort, "${name}")
	@postCreate("info.scce.cinco.product.ml.process.IOPortPostCreate")
	@preDelete("info.scce.cinco.product.ml.process.IOPortPreDelete")
	@disable(resize,move)
	@palette("Closure")
	node StartOutputPort extends InferredPort {
		outgoingEdges ({InferredStartDataFlow}[0,*])
	}
	
	@style(inputPort, "${name}")
	@postCreate("info.scce.cinco.product.ml.process.IOPortPostCreate")
	@preDelete("info.scce.cinco.product.ml.process.IOPortPreDelete")
	@disable(resize,move)
	@palette("Closure")
	node EndInputPort extends InferredPort {
		incomingEdges ({InferredEndDataFlow}[0,1])
	}
	
	abstract node ConstantPort {
		outgoingEdges ({InferredStartDataFlow}[0,*])
	}
	
	@style(constant, "${value}")
	@palette("Constants")
	node TextConstant extends ConstantPort {
		@pyroDirectEdit
		attr EString as value
	}
	
	@style(constant, "${value}")
	@palette("Constants")
	node NumberConstant extends ConstantPort {
		@pyroDirectEdit
		attr EString as value
	}
	
	@style(constant, "${value}")
	@palette("Constants")
	node BoolConstant extends ConstantPort {
		attr EBoolean as value
	}
	
	@style(constant, "${path}")
	@palette("Constants")
	node FilePathConstant extends ConstantPort {
		@pyroDirectEdit
		attr EString as path
	}
	
	@style(constant, "List of ${entries.length}")
	@palette("Constants")
	node ListConstant extends ConstantPort {
		attr Entry as entries[0,*]
		@propertiesViewHidden
		attr Entry as entry
	}
	
	abstract type Entry {}
	
	type TextEntry extends Entry {
		attr EString as value
	}
	
	type NumberEntry extends Entry {
		attr EString as value
	}
	
	type BoolEntry extends Entry {
		attr EBoolean as value
	}
	
	type FilePathEntry extends Entry {
		attr EString as value
	}
	
	


	@style(dataFlow)
	edge DataFlow { }
	
	abstract edge InferredDataFlow {}
	
	@style(dataFlow)
	edge InferredStartDataFlow extends InferredDataFlow {}
	
	@style(dataFlow)
	edge InferredEndDataFlow extends InferredDataFlow {}
}

