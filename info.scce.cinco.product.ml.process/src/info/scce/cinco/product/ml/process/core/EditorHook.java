package info.scce.cinco.product.ml.process.core;

import info.scce.pyro.core.EditorGridLayout;

public class EditorHook extends info.scce.pyro.api.PyroEditorHook {
	public void execute(de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGrid grid) {

		// explorer
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem explorerItem = getEditorLayoutService()
				.createGridArea(grid, 0L, 0L, 3L, 3L);
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget explorerWidget = getEditorLayoutService()
				.createWidget(grid, explorerItem, "Explorer", "explorer");
		explorerItem.getwidgets_PyroEditorWidget().add(explorerWidget);

		// canvas
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem canvasItem = getEditorLayoutService()
				.createGridArea(grid, 3L, 0L, 6L, 6L);
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget canvasWidget = getEditorLayoutService()
				.createWidget(grid, canvasItem, "Canvas", "canvas");
		canvasItem.getwidgets_PyroEditorWidget().add(canvasWidget);

		// properties
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem propertiesItem = getEditorLayoutService()
				.createGridArea(grid, 3L, 6L, 6L, 3L);
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget propertiesWidget = getEditorLayoutService()
				.createWidget(grid, propertiesItem, "Properties", "properties");
		propertiesItem.getwidgets_PyroEditorWidget().add(propertiesWidget);

		// palette
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem paletteItem = getEditorLayoutService()
				.createGridArea(grid, 9L, 0L, 3L, 3L);
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget paletteWidget = getEditorLayoutService()
				.createWidget(grid, paletteItem, "Palette", "palette");
		paletteItem.getwidgets_PyroEditorWidget().add(paletteWidget);

		// checks
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem checksItem = getEditorLayoutService()
				.createGridArea(grid, 9L, 3L, 3L, 3L);
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget checksWidget = getEditorLayoutService()
				.createWidget(grid, checksItem, "Checks", "checks");
		checksItem.getwidgets_PyroEditorWidget().add(checksWidget);

		// Ecore
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorGridItem ecoreItem = getEditorLayoutService()
				.createGridArea(grid, 0L, 3L, 3L, 3L);
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget ecoreWidget = getEditorLayoutService()
				.createWidget(grid, ecoreItem, "Ecore", "plugin_ecore");
		ecoreItem.getwidgets_PyroEditorWidget().add(ecoreWidget);

		// put rest to available
		
		//Map
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget pluginMapWidget = getEditorLayoutService().createWidget(grid, null, "Map", "map");
		grid.getavailableWidgets_PyroEditorWidget().add(pluginMapWidget);

		//cmd history
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget cmdHistoryWidget = getEditorLayoutService().createWidget(grid, null, "Command History", "command_history");
		grid.getavailableWidgets_PyroEditorWidget().add(cmdHistoryWidget);
		
		//shared
		final de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroEditorWidget pluginSharedWidget = getEditorLayoutService().createWidget(grid, null, "Shared", "plugin_shared");
		grid.getavailableWidgets_PyroEditorWidget().add(pluginSharedWidget);

	}
}
