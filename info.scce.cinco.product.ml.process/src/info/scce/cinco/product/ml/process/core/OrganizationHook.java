package info.scce.cinco.product.ml.process.core;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroOrganization; 
import info.scce.pyro.api.PyroOrganizationHook;

public class OrganizationHook extends info.scce.pyro.api.PyroOrganizationHook{
	
	public void execute(PyroOrganization organization) {	 
		  organization.getstyle().setnavBgColor("3c7982");		 
		  organization.getstyle().setnavTextColor("ffffff");		 
		  organization.getstyle().setbodyBgColor("474747");		 
		  organization.getstyle().setbodyTextColor("ffffff");		 
		  organization.getstyle().setprimaryBgColor("3c7982");		 
		  organization.getstyle().setprimaryTextColor("ffffff");
		}


}
