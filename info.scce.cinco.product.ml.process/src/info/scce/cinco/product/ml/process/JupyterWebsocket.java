package info.scce.cinco.product.ml.process;

import javax.websocket.*;
import javax.websocket.ClientEndpointConfig.Builder;
import javax.websocket.ClientEndpointConfig.Configurator;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
@ClientEndpoint
public class JupyterWebsocket extends Endpoint{

    Session userSession = null;

    public JupyterWebsocket(URI endpointURI,String token) {
        System.out.println(endpointURI.toString());
        Builder configBuilder = ClientEndpointConfig.Builder.create();
        configBuilder.configurator(new Configurator() {
            @Override
            public void beforeRequest(Map<String, List<String>> headers) {
                headers.put("Authorization", Collections.singletonList("token "+token));
            }
        });
        ClientEndpointConfig clientConfig = configBuilder.build();
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.setAsyncSendTimeout(10000);
            container.setDefaultMaxSessionIdleTimeout(10000);
            container.connectToServer(this, clientConfig, endpointURI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }




    /**
     * Send a message.
     *
     * @param message
     */
    public void sendMessage(String message) {
        this.userSession.getAsyncRemote().sendText(message);
    }

    @Override()
    public void onError(Session session, Throwable thr) {
        System.out.println(session.getId());
        thr.printStackTrace();
    }

    @Override
    public void onOpen(Session session, EndpointConfig config) {
        System.out.println("opening websocket");
        this.userSession = session;

    }
}
