package info.scce.cinco.product.ml.process;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProjectServiceConnect_Jupyter_Account;
import de.ls5.dywa.generated.entity.jupyter.Function;
import de.ls5.dywa.generated.entity.jupyter.FunctionGroup;
import de.ls5.dywa.generated.entity.jupyter.Parameter;
import info.scce.pyro.api.PyroProjectHook;
import info.scce.pyro.core.JupyterController;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.ws.rs.client.*;
import javax.ws.rs.core.Link;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class Synchronize extends PyroProjectHook{

    private JupyterUtil util = new JupyterUtil();

    public boolean canExecute(PyroProject project) {
        return util.getService(project) != null;
    }

    @Override
    public void execute(PyroProject project) {

        PyroProjectServiceConnect_Jupyter_Account service = util.getService(project);

        if(service == null) {
            return;
        }
        
        String token = service.getToken();
        String username = service.getUsername();
        /*
         * USER
         */
        JsonObject userObj = util.checkUser(username,token);

        /*
        Check Server status
         */
        if(userObj.isNull("server")) {
            util.startServer(username,token);
        } else {
            System.out.println("Server is running");
        }


        List<String> elements = null;
        try {
            System.out.println("Start Function reading");
            elements = getFunctions(username,"",token);
            elements.forEach(System.out::println);


        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        if(elements == null) {
            return;
        }

        List<PyFile> pyFiles = null;
        try {
            System.out.println("Fetch source code");
            pyFiles = extractFunctions(username, elements,token);
            pyFiles.forEach(System.out::println);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(pyFiles == null) {
            return;
        }

        /*
        synchronize ecore
         */

        System.out.println("Start Ecore sync");

        JupyterController ctrl = (JupyterController) this.getBundles().stream().filter(n->n instanceof JupyterController).findFirst().get();
        de.ls5.dywa.generated.entity.jupyter.Jupyter lib = null;
        java.util.Set<de.ls5.dywa.generated.entity.jupyter.Jupyter> files = ctrl.collectProjectFiles(project);
        if(!files.isEmpty()) {
            lib = files.iterator().next();
        } else {
            lib = ctrl.getjupyterController().create("Jupyter");
            lib.setextension("lib");
            lib.setfilename("Jupyter");
            project.getfiles_PyroFile().add(lib);
        }
        List<FunctionGroup> fgtoDelete = new LinkedList<>();
        for(FunctionGroup fg:lib.getfunctiongroups_FunctionGroup()) {
            if(pyFiles.stream().noneMatch(n->n.filename.equals(fg.getname()))) {
                fgtoDelete.add(fg);
                List<Function> funs = new LinkedList<>(fg.getfunctions_Function());
                funs.forEach(n->{
                    List<Parameter> inputs = new LinkedList<>(n.getinputs_Parameter());
                    inputs.forEach(i->ctrl.getParameterController().deleteWithIncomingReferences(i));
                    if(n.getoutput()!=null) {
                        ctrl.getParameterController().deleteWithIncomingReferences(n.getoutput());
                    }
                    ctrl.getFunctionController().deleteWithIncomingReferences(n);
                });
            }
        }
        for(FunctionGroup n:fgtoDelete)
        {
            lib.getfunctiongroups_FunctionGroup().remove(n);
            ctrl.getFunctionGroupController().delete(n);
        }

        for(PyFile file:pyFiles) {
            Optional<FunctionGroup> functionGroup = lib.getfunctiongroups_FunctionGroup().stream().filter(n -> n.getname().equals(file.filename)).findAny();
            FunctionGroup fg = null;
            if(functionGroup.isPresent()) {
                fg = functionGroup.get();
            } else {
                fg = ctrl.getFunctionGroupController().create(file.filename);
                fg.setname(file.filename);
                lib.getfunctiongroups_FunctionGroup().add(fg);
            }

            List<PyFunction> funs = file.functions;

            //remove no more present
            List<Function> funToDelete =  fg.getfunctions_Function().stream().filter(n->funs.stream().noneMatch(f->f.functionName.equals(n.getname()))).collect(Collectors.toList());
            funToDelete.forEach(n->{
                List<Parameter> inputs= new LinkedList<>(n.getinputs_Parameter());
                inputs.forEach(p->ctrl.getParameterController().deleteWithIncomingReferences(p));
                if(n.getoutput() != null) {
                    ctrl.getParameterController().deleteWithIncomingReferences(n.getoutput());
                }
                ctrl.getFunctionController().deleteWithIncomingReferences(n);

            });

            //Add functions to lib
            for(PyFunction f:funs) {
                //add new
                Optional<Function> optional = fg.getfunctions_Function().stream().filter(n -> n.getname().equals(f.functionName)).findFirst();
                Function newFun  = null;
                if(!optional.isPresent()) {
                    //function unkown
                    newFun = ctrl.getFunctionController().create(f.label);
                    fg.getfunctions_Function().add(newFun);
                } else {
                    newFun = optional.get();
                }
                newFun.setname(f.functionName);
                newFun.setdocumentation(f.documentation);
                newFun.setattr_import(f.importStatement);

                // sync inputs
                //delete removed parameter
                List<Parameter> toDelete =  newFun
                        .getinputs_Parameter()
                        .stream()
                        .filter(n->f.inputs
                                .stream()
                                .noneMatch(i->i.name.equals(n.getname()) && i.typeName.equals(n.gettypeName()))
                        ).collect(Collectors.toList());
                toDelete.forEach(n-> ctrl.getParameterController().deleteWithIncomingReferences(n));

                for(PyParameter parameter:f.inputs) {
                    Optional<Parameter> optionalParam = newFun.getinputs_Parameter().stream().filter(n -> n.getname().equals(parameter.name) && n.gettypeName().equals(parameter.typeName)).findFirst();
                    if(!optionalParam.isPresent()) {
                        //not present add new
                        Parameter p = ctrl.getParameterController().create("Param_"+parameter.name);
                        p.setname(parameter.name);
                        p.settypeName(parameter.typeName);
                        newFun.getinputs_Parameter().add(p);
                    }
                    //else no sync needed
                }

                //sync outputs
                if(newFun.getoutput() != null && f.output != null) {
                    if(!(newFun.getoutput().gettypeName().equals(f.output.typeName) && newFun.getoutput().getname().equals(f.output.name))) {
                        //changed output delete and renew
                        ctrl.getParameterController().deleteWithIncomingReferences(newFun.getoutput());
                        Parameter p = ctrl.getParameterController().create(f.output.name);
                        p.setname(f.output.name);
                        p.settypeName(f.output.typeName);
                        newFun.setoutput(p);
                    }
                    // else no sync needed

                }
                else if(newFun.getoutput() != null && f.output == null) {
                    ctrl.getParameterController().deleteWithIncomingReferences(newFun.getoutput());
                }
                else if(newFun.getoutput() == null && f.output != null) {
                    Parameter p = ctrl.getParameterController().create(f.output.name);
                    p.setname(f.output.name);
                    p.settypeName(f.output.typeName);
                    newFun.setoutput(p);
                }
            }
        }

    }

    private List<String> getFunctions(String username,String path,String token) throws ExecutionException, InterruptedException {

        List<String> innerFolders = new LinkedList<>();
        // read all files
        JsonObject resp = util.get(JupyterUtil.NOTEBOOK_URL+"/"+username+"/api/contents"+path,token).get();

        if(resp.containsKey("content") && resp.getJsonArray("content") != null) {
            for(JsonObject inner:resp.getJsonArray("content").stream().filter(n->n instanceof JsonObject).map(n->(JsonObject)n).collect(Collectors.toList())) {
                if(
                        inner.containsKey("type")
                    && !inner.isNull("type")
                    && inner.containsKey("mimetype")
                    && !inner.isNull("mimetype")
                    && inner.getString("type").equals("file")
                    && inner.getString("mimetype").equals("text/x-python")
                ) {
                    //.py file
                    innerFolders.add(path+"/"+inner.getString("name"));
                }
                if(
                        inner.containsKey("type")
                    && !inner.isNull("type")
                    && inner.getString("type").equals("notebook")
                ) {
                    //jupyter notebook
                    innerFolders.add(path+"/"+inner.getString("name"));

                }
                if(
                        inner.containsKey("type")
                    && !inner.isNull("type")
                    && inner.getString("type").equals("directory")
                ) {

                    innerFolders.addAll(getFunctions(username, path+"/"+inner.getString("name"),token));
                }
            }
        }
        return innerFolders;
    }



    private String noExtension(String s) {
        return s.substring(0,s.lastIndexOf("."));
    }



    private List<PyFile> extractFunctions(String username, List<String> files,String token) throws ExecutionException, InterruptedException {
        List<PyFile> pyFiles = new LinkedList<>();
        for(String f:files) {
            JsonObject resp = util.get(JupyterUtil.NOTEBOOK_URL+"/"+username+"/api/contents"+f,token).get();
            if(f.endsWith(".py")) {
                processSourceCode(f,resp.getString("content"),pyFiles,false);

            }
            if(f.endsWith(".ipynb")) {
                for(JsonObject c:resp.getJsonObject("content").getJsonArray("cells").stream().filter(n->n instanceof JsonObject).map(n->(JsonObject)n).filter(n->n.containsKey("cell_type") && n.getString("cell_type").equals("code")).collect(Collectors.toList())) {
                    processSourceCode(f,c.getString("source"),pyFiles,true);
                }
            }

        }

        return pyFiles;
    }

    private void processSourceCode(String path,String code,List<PyFile> pyFiles,boolean isNotebook) {
        String lines[] = code.split("\\r?\\n");
        String fileName = noExtension(path.substring(path.lastIndexOf("/")+1));
        List<PyFunction> pyFunctions = new LinkedList<>();
        for(int i = 0;i<lines.length;i++) {
            if(lines[i].startsWith("#")) {
                String comment = lines[i].replaceAll(" ", "");
                if(comment.startsWith("#Method:")) {
                    PyFunction pf = new PyFunction();
                    pf.documentation = lines[i];
                    pf.label = comment.substring(comment.lastIndexOf("#Method:") + 8);
                    if(isNotebook) {
                        pf.functionName = pf.label;
                        pf.importStatement = "from ipynb.fs.full."+noExtension(path.replaceAll("/",""))+" import "+pf.functionName;
                    } else {
                        pf.importStatement = "import "+noExtension(path.substring(1).replaceAll("/","\\."))+" as "+fileName;
                        pf.functionName = fileName+"."+pf.label;
                    }

                    if(lines.length >= i+1 && lines[i+1].startsWith("# Inputs:")) {
                        pf.documentation += "\n"+lines[i+1];
                        String inputs = lines[i+1].replaceAll(" ", "");
                        String list = inputs.substring(inputs.lastIndexOf("#Inputs:") + 8);
                        if(list.contains(",")) {
                            for(String p:list.split(",")) {
                                if(p.contains(":")) {
                                    pf.inputs.add(new PyParameter(p.split(":")[0],p.split(":")[1]));
                                }
                            }
                        } else {
                            if(list.contains(":")) {
                                pf.inputs.add(new PyParameter(list.split(":")[0],list.split(":")[1]));
                            }
                        }

                        if(lines.length >= i+2 &&  lines[i+2].startsWith("# Output:")) {
                            pf.documentation += "\n"+lines[i+2];
                            String output = lines[i+2].replaceAll(" ", "");
                            String outputParam = output.substring(output.lastIndexOf("#Output:") + 8);
                            if(outputParam.contains(":")) {
                                pf.output = new PyParameter(outputParam.split(":")[0],outputParam.split(":")[1]);
                            }
                        }
                        pyFunctions.add(pf);
                    }
                }

            }
        }

        if(!pyFunctions.isEmpty()) {
            PyFile f = new PyFile();
            f.filename = fileName;
            f.functions.addAll(pyFunctions);
            pyFiles.add(f);
        }
    }

}
class PyFile {
    String filename;
    List<PyFunction> functions = new LinkedList<>();
    @Override()
    public String toString() {
        return filename+"->"+functions.stream().map(PyFunction::toString).collect(Collectors.joining(","));
    }
}

class PyFunction {
    String label;
    String importStatement;
    String functionName;
    List<PyParameter> inputs = new LinkedList<>();
    PyParameter output;
    String documentation;

    @Override()
    public String toString() {
        return functionName+"("+inputs.stream().map((n)->n.name+":"+n.typeName).collect(Collectors.joining(","))+")->"+(output==null?"void":(output.name+":"+output.typeName));
    }
}

class PyParameter {
    String name;
    String typeName;
    PyParameter(String name,String typeName) {
        this.name = name;
        this.typeName = typeName;
    }
}