package info.scce.cinco.product.ml.process;

import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProject;
import de.ls5.dywa.generated.entity.info.scce.pyro.core.PyroProjectServiceConnect_Jupyter_Account;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.InvocationCallback;
import java.io.StringReader;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Author zweihoff
 */
public class JupyterUtil {
    static final String BASE_URL = "https://ls5vs026.cs.tu-dortmund.de";
    static final String HUB_URL = BASE_URL+"/hub/api";
    static final String NOTEBOOK_URL = BASE_URL+"/user";
    static final String WS_URL = "wss://ls5vs026.cs.tu-dortmund.de";

    PyroProjectServiceConnect_Jupyter_Account getService(PyroProject project) {
        Optional<PyroProjectServiceConnect_Jupyter_Account> first = project.getservices_PyroProjectService()
                .stream().filter(n -> n instanceof PyroProjectServiceConnect_Jupyter_Account)
                .map(n -> (PyroProjectServiceConnect_Jupyter_Account) n)
                .findFirst();
        if(!first.isPresent()) {
            return null;
        }
        return first.get();
    }

    JsonObject checkUser(String username,String token) {
        JsonObject userObj = null;
        try {

            return this.get(JupyterUtil.HUB_URL+"/users/"+username,token).get();
        } catch(Exception e) {
            //user not found create one
            try {
                return this.post(JupyterUtil.HUB_URL+"/users/"+username,token,null).get();
            } catch (InterruptedException | ExecutionException e1) {
                e1.printStackTrace();
                return null;
            }
        }

    }

    void startServer(String username,String token) {
        System.out.println("Server is not running");
        try {
            this.post(JupyterUtil.HUB_URL+"/users/"+username+"/server",token,null).get();
            System.out.println("Server started");
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    CompletableFuture<JsonObject> post(String url, String token, Object payload) {
        System.out.println("POST: "+url);
        CompletableFuture<JsonObject> completableFuture = new CompletableFuture<>();
        ResteasyClientBuilder.newClient().target(url)
                .request()
                .header("Authorization","token "+token)
                .header("content-type", "application/json")
                .async()
                .post(payload==null? Entity.text(""):Entity.json(payload),new InvocationCallback<String>() {
                    @Override
                    public void completed(String resp) {
                        if(resp == null || resp.isEmpty()) {
                            System.out.println("No Response");
                            completableFuture.complete(null);
                            return;
                        }
                        //System.out.println("Response: "+resp);
                        JsonReader reader = Json.createReader(new StringReader(resp));
                        completableFuture.complete(reader.readObject());
                    }

                    @Override
                    public void failed(Throwable throwable) {
                        System.out.println("POST FAILED : "+url);
                        completableFuture.completeExceptionally(throwable);
                    }
                });
        return completableFuture;
    }

    CompletableFuture<JsonObject> get(String url, String token) {
        System.out.println("GET: "+url);
        CompletableFuture<JsonObject> completableFuture = new CompletableFuture<>();
        ResteasyClientBuilder.newClient().target(url)
                .request()
                .header("Authorization","token "+token)
                .header("content-type", "application/json")
                .async()
                .get(new InvocationCallback<String>() {
                    @Override
                    public void completed(String resp) {
                        //System.out.println("Response: "+resp);
                        JsonReader reader = Json.createReader(new StringReader(resp));
                        completableFuture.complete(reader.readObject());
                    }

                    @Override
                    public void failed(Throwable throwable) {
                        completableFuture.completeExceptionally(throwable);
                    }
                });
        return completableFuture;
    }
}
